### 실행 절차

- 첨부 파일에서 Sample Raw Data, Sample Transfer Function Download.
- Windows - VolumeRenderingAssetBuilder를 실행하여 Texture, Gradient Texture를 생성.
- Demo Scene 실행.
- Volume Rendering Game Object에 데이터 업로드 및 렌더링 설정 세팅
- 실행.