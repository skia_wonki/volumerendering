﻿using System.IO;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

namespace VolumeRendering
{
    public class VolumeAssetBuilder : EditorWindow {

        [MenuItem("Window/VolumeAssetBuilder")]
        static void Init()
        {
            var window = EditorWindow.GetWindow(typeof(VolumeAssetBuilder));
            window.Show();
        }

        string inputPath, outputPath, gradientOutputPath;
        int width = 256, height = 256, depth = 256;
        Object asset;

        void OnEnable()
        {
            inputPath = "Assets/MRI.256x256x256.raw";
            outputPath = "Assets/MRI.asset";
            gradientOutputPath = "Assets/MRI_GRADIENT.asset";
        }

        void OnGUI()
        {
            const float headerSize = 120f;
            
            using(new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Label("Input pvm raw file path", GUILayout.Width(headerSize));
                asset = EditorGUILayout.ObjectField(asset, typeof(Object), true);
                inputPath = AssetDatabase.GetAssetPath(asset);
            }

            using(new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Label("Width", GUILayout.Width(headerSize));
                width = EditorGUILayout.IntField(width);
            }

            using(new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Label("Height", GUILayout.Width(headerSize));
                height = EditorGUILayout.IntField(height);
            }

            using(new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Label("Depth", GUILayout.Width(headerSize));
                depth = EditorGUILayout.IntField(depth);
            }

            using(new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Label("Output path", GUILayout.Width(headerSize));
                outputPath = EditorGUILayout.TextField(outputPath);
            }

            using (new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Label("Gradient Output path", GUILayout.Width(headerSize));
                gradientOutputPath = EditorGUILayout.TextField(gradientOutputPath);
            }

            if (GUILayout.Button("Build"))
            {
                Build(inputPath, outputPath, width, height, depth);
                BuildGradient(inputPath, gradientOutputPath, width, height, depth);
            }
        }

        void Build(
            string inputPath, 
            string outputPath, 
            int width,
            int height,
            int depth 
        )
        {
            if (!File.Exists(inputPath))
            {
                Debug.LogWarning(inputPath + " is not exist.");
                return;
            }

            var volume = Build(inputPath, width, height, depth);
            AssetDatabase.CreateAsset(volume, outputPath);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        void BuildGradient(
            string inputPath,
            string outputPath,
            int width,
            int height,
            int depth
        )
        {
            if (!File.Exists(inputPath))
            {
                Debug.LogWarning(inputPath + " is not exist.");
                return;
            }

            var volume = BuildGradient(inputPath, width, height, depth);
            AssetDatabase.CreateAsset(volume, outputPath);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        public static Texture3D Build(string path, int width, int height, int depth)
        {
            var max = width * height * depth;
            var tex  = new Texture3D(width, height, depth, TextureFormat.ARGB32, false);
            tex.wrapMode = TextureWrapMode.Clamp;
            tex.filterMode = FilterMode.Bilinear;
            tex.anisoLevel = 0;

            using(var stream = new FileStream(path, FileMode.Open))
            {
                var len = stream.Length;
                if(len != max)
                {
                    Debug.LogWarning(path + " doesn't have required resolution");
                }

                int i = 0;
                Color[] colors = new Color[max];
                float inv = 1f / 255.0f;
                for(i = 0; i < stream.Length; i++)
                {
                    if(i == max)
                    {
                        break;
                    }
                    int v = stream.ReadByte();
                    float f = v * inv;
                    colors[i] = new Color(f, f, f, f);
                }
                tex.SetPixels(colors);
                tex.Apply();
            }
            return tex;
        }

        public static Texture3D BuildGradient(string path, int width, int height, int depth)
        {
            var max = width * height * depth;
            var tex = new Texture3D(width, height, depth, TextureFormat.ARGB32, false);
            tex.wrapMode = TextureWrapMode.Clamp;
            tex.filterMode = FilterMode.Bilinear;
            tex.anisoLevel = 0;

            using (var stream = new FileStream(path, FileMode.Open))
            {
                var len = stream.Length;
                if (len != max)
                {
                    Debug.LogWarning(path + " doesn't have required resolution");
                }

                int i = 0;
                Color[] colors = new Color[max];
                float[] data = new float[max];

                float inv = 1f / 255.0f;
                for (i = 0; i < stream.Length; i++)
                {
                    if (i == max)
                    {
                        break;
                    }

                    int v = stream.ReadByte();
                    float f = v * inv;
                    data[i] = f;
                }

                for (int z = 1; z < depth - 1; z++)
                {
                    for (int y = 1; y < height - 1; y++)
                    {
                        for (int x = 1; x < width - 1; x++)
                        {
                            float xDiff = data[z * width * height + y * width + (x - 1)] - data[z * width * height + y * width + (x + 1)];
                            float yDiff = data[z * width * height + (y - 1) * width + x] - data[z * width * height + (y + 1) * width + x];
                            float zDiff = data[(z - 1) * width * height + y * width + x] - data[(z + 1) * width * height + y * width + x];

                            Vector3 gradient = new Vector3(xDiff, yDiff, zDiff);
                            gradient = gradient.normalized;

                            colors[z * width * height + y * width + x] = new Color(gradient.x, gradient.y, gradient.z);
                        }
                    }
                }

                tex.SetPixels(colors);
                tex.Apply();
            }
            return tex;
        }

    }
}



