﻿
using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using UnityEngine;

public enum RenderingMethod
{
    DVR,
    MIP,
    SURF
};

public enum TransferFunctionMode
{
    TF1D,
    TF2D
};

namespace VolumeRendering
{

    [RequireComponent (typeof(MeshRenderer), typeof(MeshFilter))]
    public class VolumeRendering : MonoBehaviour {

        [SerializeField] protected Shader shader;
        protected Material material;

        [Range(0f, 1f)] public float threshold = 0.5f;
        [Range(0.5f, 5f)] public float intensity = 1.5f;
        [Range(0f, 1f)] public float sliceXMin = 0.0f, sliceXMax = 1.0f;
        [Range(0f, 1f)] public float sliceYMin = 0.0f, sliceYMax = 1.0f;
        [Range(0f, 1f)] public float sliceZMin = 0.0f, sliceZMax = 1.0f;
        public Quaternion axis = Quaternion.identity;

        public Texture volume;
        public Texture gradient;
        public Texture tf1d;
        public Texture tf2d;

        public RenderingMethod method = RenderingMethod.DVR;
        public bool lightOn = false;
        public TransferFunctionMode tf = TransferFunctionMode.TF1D;

        [Range(0f, 0.5f)] public float visibilityMin = 0.0f;
        [Range(0.5f, 1.0f)] public float visibilityMax = 1.0f;

        protected virtual void Start () {
            material = new Material(shader);
            GetComponent<MeshFilter>().sharedMesh = Build();
            GetComponent<MeshRenderer>().sharedMaterial = material;
        }
        
        protected void Update () {
            material.SetTexture("_DataTex", volume);
            material.SetFloat("_Threshold", threshold);
            material.SetFloat("_Intensity", intensity);
            material.SetVector("_SliceMin", new Vector3(sliceXMin, sliceYMin, sliceZMin));
            material.SetVector("_SliceMax", new Vector3(sliceXMax, sliceYMax, sliceZMax));
            material.SetMatrix("_AxisRotationMatrix", Matrix4x4.Rotate(axis));

            material.SetTexture("_GradientTex", gradient);

            switch (method)
            {
                case RenderingMethod.DVR:
                    material.EnableKeyword("MODE_DVR");
                    material.DisableKeyword("MODE_MIP");
                    material.DisableKeyword("MODE_SURF");
                    break;
                case RenderingMethod.MIP:
                    material.DisableKeyword("MODE_DVR");
                    material.EnableKeyword("MODE_MIP");
                    material.DisableKeyword("MODE_SURF");
                    break;
                case RenderingMethod.SURF:
                    material.DisableKeyword("MODE_DVR");
                    material.DisableKeyword("MODE_MIP");
                    material.EnableKeyword("MODE_SURF");
                    break;
            }

            switch (tf)
            {
                case TransferFunctionMode.TF1D:
                    material.SetTexture("_TFTex", tf1d);
                    material.EnableKeyword("TF2D_ON");
                    break;
                case TransferFunctionMode.TF2D:
                    material.SetTexture("_TFTex", tf2d);
                    material.DisableKeyword("TF2D_ON");
                    break;
            }

            if (lightOn) material.EnableKeyword("LIGHTING_ON");
            else material.DisableKeyword("LIGHTING_ON");

            material.SetFloat("_MinVal", visibilityMin);
            material.SetFloat("_MaxVal", visibilityMax);
        }

        Mesh Build() {
            var vertices = new Vector3[] {
                new Vector3 (-0.5f, -0.5f, -0.5f),
                new Vector3 ( 0.5f, -0.5f, -0.5f),
                new Vector3 ( 0.5f,  0.5f, -0.5f),
                new Vector3 (-0.5f,  0.5f, -0.5f),
                new Vector3 (-0.5f,  0.5f,  0.5f),
                new Vector3 ( 0.5f,  0.5f,  0.5f),
                new Vector3 ( 0.5f, -0.5f,  0.5f),
                new Vector3 (-0.5f, -0.5f,  0.5f),
            };
            var triangles = new int[] {
                0, 2, 1,
                0, 3, 2,
                2, 3, 4,
                2, 4, 5,
                1, 2, 5,
                1, 5, 6,
                0, 7, 4,
                0, 4, 3,
                5, 4, 7,
                5, 7, 6,
                0, 6, 7,
                0, 1, 6
            };

            var mesh = new Mesh();
            mesh.vertices = vertices;
            mesh.triangles = triangles;
            mesh.RecalculateNormals();
            mesh.hideFlags = HideFlags.HideAndDontSave;
            return mesh;
        }

        void OnValidate()
        {
            Constrain(ref sliceXMin, ref sliceXMax);
            Constrain(ref sliceYMin, ref sliceYMax);
            Constrain(ref sliceZMin, ref sliceZMax);
        }

        void Constrain (ref float min, ref float max)
        {
            const float threshold = 0.025f;
            if(min > max - threshold)
            {
                min = max - threshold;
            } else if(max < min + threshold)
            {
                max = min + threshold;
            }
        }

        void OnDestroy()
        {
            Destroy(material);
        }

    }

}


