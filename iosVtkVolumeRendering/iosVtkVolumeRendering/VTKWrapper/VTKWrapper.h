//
//  VTKWrapper.h
//  iosVtkVolumeRendering
//
//  Created by Wonki Eun on 2020/09/29.
//

#ifndef VTKWrapper_h
#define VTKWrapper_h

#   ifdef __cplusplus
extern "C" {
#   endif

void* initRenwin();

void setVolume(void* renWin, const char* filename);
void render(void* renWin);
void resizeWin(void* renWin, float width, float height);

#   ifdef __cplusplus
}
#   endif

#endif /* VTKWrapper_h */
