//
//  VTKWrapper.cpp
//  iosVtkVolumeRendering
//
//  Created by Wonki Eun on 2020/09/29.
//

#include "VTKWrapper.h"

#include "vtk/vtkIOSRenderWindow.h"
#include "vtk/vtkIOSRenderWindowInteractor.h"

#include "vtk/vtkRenderingOpenGL2ObjectFactory.h"
#include "vtk/vtkRenderingVolumeOpenGL2ObjectFactory.h"

#include "vtk/vtkNew.h"

#include "vtk/vtkActor.h"
#include "vtk/vtkCamera.h"
#include "vtk/vtkColorTransferFunction.h"
#include "vtk/vtkConeSource.h"
#include "vtk/vtkDebugLeaks.h"
#include "vtk/vtkGlyph3D.h"
#include "vtk/vtkImageCast.h"
#include "vtk/vtkImageData.h"
#include "vtk/vtkNrrdReader.h"
#include "vtk/vtkOpenGLGPUVolumeRayCastMapper.h"
#include "vtk/vtkPiecewiseFunction.h"
#include "vtk/vtkPointData.h"
#include "vtk/vtkPolyData.h"
#include "vtk/vtkPolyDataMapper.h"
#include "vtk/vtkRTAnalyticSource.h"
#include "vtk/vtkRenderWindow.h"
#include "vtk/vtkRenderer.h"
#include "vtk/vtkSmartPointer.h"
#include "vtk/vtkSphereSource.h"
#include "vtk/vtkTextActor.h"
#include "vtk/vtkTextProperty.h"
#include "vtk/vtkVolume.h"
#include "vtk/vtkVolumeProperty.h"

#include "vtk/vtkActor2D.h"
#include "vtk/vtkCommand.h"
#include "vtk/vtkInteractorStyleMultiTouchCamera.h"
#include "vtk/vtkMath.h"
#include "vtk/vtkPoints.h"
#include "vtk/vtkPolyDataMapper.h"
#include "vtk/vtkTextMapper.h"
#include "vtk/vtkTextProperty.h"

#include "vtk/vtkDICOMImageReader.h"

#include "vtk/vtkAutoInit.h"
#include "vtk/vtkSmartPointer.h"
#include "vtk/vtkSmartVolumeMapper.h"

VTK_MODULE_INIT(vtkRenderingOpenGL2);
VTK_MODULE_INIT(vtkInteractionStyle);
VTK_MODULE_INIT(vtkRenderingVolumeOpenGL2);

extern "C" {
void* initRenwin() {
    // Register GL2 objects
    vtkObjectFactory::RegisterFactory(vtkRenderingOpenGL2ObjectFactory::New());
    vtkObjectFactory::RegisterFactory(vtkRenderingVolumeOpenGL2ObjectFactory::New());
    
    vtkIOSRenderWindow* renWin = vtkIOSRenderWindow::New();
    //renWin->DebugOn();

    // this example uses VTK's built in interaction but you could choose
    // to use your own instead.
    vtkNew<vtkIOSRenderWindowInteractor> iren;
    renWin->SetInteractor(iren.GetPointer());
    vtkNew<vtkInteractorStyleMultiTouchCamera> ismt;
    //ismt->DebugOn();
    iren->SetInteractorStyle(ismt.Get());
    iren->SetRenderWindow(renWin);
    iren->SetDesiredUpdateRate(30);
    
    return renWin;
}

void setVolume(void* renWin, const char* filename) {
    vtkIOSRenderWindow * win = static_cast<vtkIOSRenderWindow *>(renWin);
    vtkNew<vtkRenderer> renderer;
    win->AddRenderer(renderer.Get());
    win->GetInteractor()->GetInteractorStyle()->SetDefaultRenderer(renderer.Get());
    
    vtkNew<vtkNrrdReader> mi;
    mi->SetFileName(filename);
    mi->Update();
    
    double range[2];
    mi->GetOutput()->GetPointData()->GetScalars()->GetRange(range);
    
    vtkNew<vtkSmartVolumeMapper> volumeMapper;
    
    volumeMapper->SetInputConnection(mi->GetOutputPort());
    volumeMapper->Update();
    
    volumeMapper->SetAutoAdjustSampleDistances(1);
    volumeMapper->SetSampleDistance(0.5);
      
    vtkNew<vtkColorTransferFunction> ctf;
    vtkNew<vtkPiecewiseFunction> pwf;
    
    bool independentComponents = true;
    vtkNew<vtkVolumeProperty> volumeProperty;
      
    ctf->AddRGBPoint(-3024, 0.0 / 255.0, 0.0 / 255.0, 0.0 / 255.0, 0.5, 0.0);
    ctf->AddRGBPoint(143.56, 157.0 / 255.0, 91.0 / 255.0, 47.0 / 255.0, 0.5, 0.0);
    ctf->AddRGBPoint(166.22, 225.0 / 255.0, 154.0 / 255.0, 74.0 / 255.0, 0.33, 0.45);
    ctf->AddRGBPoint(214.39, 255.0 / 255.0, 255.0 / 255.0, 255.0 / 255.0, 0.5, 0.0);
    ctf->AddRGBPoint(419.74, 255.0 / 255.0, 239.0 / 255.0, 244.0 / 255.0, 0.5, 0.0);
    ctf->AddRGBPoint(3071, 211.0 / 255.0, 168.0 / 255.0, 255.0 / 255.0, 0.5, 0.0);
      
    pwf->AddPoint(-3024, 0, 0.5, 0.0);
    pwf->AddPoint(143.56, 0, 0.5, 0.0);
    pwf->AddPoint(166.22, 0.69, 0.5, 0.0);
    pwf->AddPoint(214.39, 0.70, 0.5, 0.0);
    pwf->AddPoint(419.74, 0.83, 0.5, 0.0);
    pwf->AddPoint(3071, 0.80, 0.5, 0.0);
      
    volumeProperty->SetColor(ctf.GetPointer());
    volumeProperty->SetScalarOpacity(pwf.GetPointer());
      
    volumeProperty->SetShade(1);
    volumeProperty->SetIndependentComponents(independentComponents);
    volumeProperty->SetInterpolationTypeToLinear();

    volumeMapper->SetBlendModeToComposite();
    volumeProperty->ShadeOn();
    volumeProperty->SetAmbient(0.1);
    volumeProperty->SetDiffuse(0.9);
    volumeProperty->SetSpecular(0.2);
    volumeProperty->SetSpecularPower(10.0);
    volumeProperty->SetScalarOpacityUnitDistance(0.8919);
      
    vtkNew<vtkVolume> volume;
    volume->SetMapper(volumeMapper.GetPointer());
    volume->SetProperty(volumeProperty.GetPointer());
      
    volume->SetScale(1.0, 1.0, 1.0);
    volume->SetOrigin(0, 0, 0);
      
    renderer->SetBackground2(0.2, 0.3, 0.4);
    renderer->SetBackground(0.1, 0.1, 0.1);
    renderer->GradientBackgroundOn();
    renderer->AddVolume(volume.GetPointer());
    renderer->ResetCamera();
    renderer->GetActiveCamera()->Zoom(1.4);
}

void render(void* renWin) {
    vtkIOSRenderWindow * win = static_cast<vtkIOSRenderWindow *>(renWin);
    win -> Render();
}

void resizeWin(void* renWin, float width, float height) {
    vtkIOSRenderWindow * win = static_cast<vtkIOSRenderWindow *>(renWin);
    win -> SetSize(width, height);
}
}
