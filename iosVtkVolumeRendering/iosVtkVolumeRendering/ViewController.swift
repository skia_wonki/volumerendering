//
//  ViewController.swift
//  iosVtkVolumeRendering
//
//  Created by Wonki Eun on 2020/09/29.
//

import UIKit
import GLKit
import OpenGLES

class ViewController: GLKViewController {
    var vtkWindow: UnsafeMutableRawPointer?
    var context: CVEAGLContext?
    var glkView: GLKView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        context = EAGLContext(api: .openGLES3)
        glkView = view as? GLKView
        glkView?.context = context!
        glkView?.drawableDepthFormat = .format16
        glkView?.drawableMultisample = .multisample4X
        
        vtkWindow = initRenwin()
        
        EAGLContext.setCurrent(context)
        
        resizeView()
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let filename = String(paths.first!) + "/CT-chest.nrrd"
        setVolume(vtkWindow, filename)
    }
    
    func resizeView() {
        let scale = self.view.contentScaleFactor
        let width = scale * self.view.bounds.size.width
        let height = scale * self.view.bounds.size.height
        resizeWin(vtkWindow, Float(width), Float(height))
    }
    
    override func glkView(_ view: GLKView, drawIn rect: CGRect) {
        render(vtkWindow)
    }
}

